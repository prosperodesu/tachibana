from core.contrib import app
from flask import render_template, Blueprint
from boards.models import Board, Thread

site_index = Blueprint('index', __name__, template_folder='templates')

@app.route('/')
def index():
    get_board = Board.objects()
    try:
        get_news_board = Board.objects(url='n')
        for item in get_news_board:
            board_id = item.id
        get_news = Thread.objects(board=board_id)
    except UnboundLocalError:
        get_news_board = ''
        get_news = ''

    return render_template('index.html',
                           boards=get_board,
                           news=get_news)
