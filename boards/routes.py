from core.contrib import app
from flask import render_template, \
    Blueprint, \
    redirect, \
    Response, \
    request, \
    session
from boards.forms import ThreadForm, PostForm, DeleteForm
from boards.models import Board, Thread, Post, Ban, Banners
from werkzeug.utils import secure_filename
from core.settings import ALLOWED_EXTENSIONS
import os
from PIL import Image
from datetime import datetime
import crypt
import hashlib
import re
import random
from netaddr import IPAddress
site_boards = Blueprint('board', __name__, template_folder='templates')

def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1] in ALLOWED_EXTENSIONS


@app.route('/<url>', methods=['GET', 'POST'])
def board(url):
    client_ip = IPAddress(request.remote_addr)
    form = ThreadForm()
    get_all_boards = Board.objects()
    get_board = Board.objects(url=url)
    th_id = None

    for item in get_board:
        board_id = str(item.id)

    get_thread = Thread.objects(board__in=[board_id])

    for item in get_thread:
        th_id = str(item.id)


    get_posts = Post.objects(thread=th_id)
    count = len(get_thread)

    if form.validate_on_submit():
        name = form.name.data
        subject = form.subject.data
        content = form.content.data
        file = request.files['file']
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        image = open(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        pattern = '[A-z]*[!]\w*'
        code = re.search(pattern, str(name))
        new_thread = Thread(subject=subject,
                            content=content,
                            pub_date=datetime.now(),
                            ip=int(client_ip))
        if code:
            salt = 'tr/:;<=>?@[//]^_`/ABCDEFGabcdef/'
            trip = code.group()
            trip_split = trip.split('!')
            password = trip_split[1]
            name = trip_split[0]
            tripcode = crypt.crypt(password, salt)
            new_thread.tripcode = '!' + tripcode[:10]

            new_thread.creator=name
            session['name'] = '%s!%s' % (name, password)
        else:
            new_thread.creator=name
            session['name'] = '%s' % name
        pil_img = Image.open(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        img_size = os.stat(os.path.join(app.config['UPLOAD_FOLDER'], filename)).st_size

        width, height = pil_img.size
        new_thread.img_height = height
        new_thread.img_width = width
        new_thread.img_size = str(img_size)
        if width and height > 150:
            width = width / 100
            height = height / 100
            new_thread.thumb_width = width * 20
            new_thread.thumb_height = height * 20
        new_thread.file.put(image, content_type='image/jpeg')
        new_thread.save()
        item_key = str(new_thread.id)
        Thread.objects(id=item_key).update_one(push__board=board_id)
        os.remove(os.path.join(app.config['UPLOAD_FOLDER'], filename))
        thread_url = '%s/thread/%s' % (url, new_thread.number)
        return redirect(thread_url)

    is_banned = False

    try:
        form = ThreadForm(name=session['name'])
        check_banned_list = Ban.objects(ip=int(client_ip))

        if check_banned_list:
            is_banned = True

        if is_banned:
            return render_template('board.html',
                                   ban=check_banned_list,
                                   post=get_posts,
                                   all_boards = get_all_boards,
                                   board = get_board,
                                   thread = get_thread,
                                   name=session['name'])
        else:
            return render_template('board.html',
                                   form=form,
                                   thread = get_thread,
                                   post=get_posts,
                                   all_boards = get_all_boards,
                                   board = get_board,
                                   name=session['name'])
    except KeyError:
         if is_banned:
            return render_template('board.html',
                                   ban=check_banned_list,
                                   thread = get_thread,
                                   post=get_posts,
                                   all_boards = get_all_boards,
                                   board = get_board,
                                   name=session['name'])
         else:
            return render_template('board.html',
                                   form=form,
                                   thread = get_thread,
                                   all_boards = get_all_boards,
                                   board = get_board,
                                   post=get_posts)


@app.route('/<url>/thread/<number>', methods=['GET', 'POST'])
def thread(url, number):
    client_ip = IPAddress(request.remote_addr)
    form = PostForm()
    delete_form = DeleteForm()
    get_posts_number = Post.objects()
    get_all_boards = Board.objects()
    get_board = Board.objects(url=url)

    for item in get_board:
        board_key = item.id

    get_thread = Thread.objects(board__in=[board_key], number=int(number))


    for item in get_thread:
        th_id = str(item.id)
    get_posts = Post.objects(thread__in=[th_id]).order_by('+pub_date')
    count = len(get_posts_number)
    post_id = count+1
    if form.validate_on_submit():
        name = form.name.data
        post = form.content.data
        password = form.password.data
        file = request.files['file']
        pattern = '[A-z]*[!]\w*'
        code = re.search(pattern, str(name))

        post_password_hash = hashlib.sha224()
        post_password = password
        post_password_hash.update(password)
        hash_password = post_password_hash.hexdigest()

        new_post = Post(content=post,
                        password=hash_password,
                        pub_date=datetime.now(),
                        ip=int(client_ip))
        if code:
            salt = 'tr/:;<=>?@[//]^_`/ABCDEFGabcdef/'
            trip = code.group()
            trip_split = trip.split('!')
            password = trip_split[1]
            name = trip_split[0]
            tripcode = crypt.crypt(password, salt)
            new_post.tripcode = '!' + tripcode[:10]

            new_post.creator=name
            session['name'] = '%s!%s' % (name, password)
        else:
            new_post.creator=name
            session['name'] = '%s' % name
        if file and allowed_file(file.filename):
            filename = secure_filename(file.filename)
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            image = open(os.path.join(app.config['UPLOAD_FOLDER'], filename), 'rb')
            pil_img = Image.open(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            img_size = os.stat(os.path.join(app.config['UPLOAD_FOLDER'], filename)).st_size
            new_post.img_size = str(img_size)
            width, height = pil_img.size
            new_post.img_width = width
            new_post.img_height = height
            if width and height > 150:
                width = width / 100
                height = height / 100
                new_post.thumb_width = width * 20
                new_post.thumb_height = height * 20
            new_post.file.put(image, content_type='image/jpeg')

        for item in get_thread:
            key = str(item.id)
        new_post.save()
        Post.objects(number=post_id).update_one(push__thread=key)

        Thread.objects(id=key).update_one(push__reply=key)
        Thread.objects(id=key).update_one(set__bump_date=datetime.now())

        redirect_path = '%s/thread/%s#%s' % (url, number, post_id)
        return redirect(redirect_path)

    if delete_form.is_submitted():
        post_password_hash = hashlib.sha224()
        post_password = delete_form.password.data
        post_password_hash.update(post_password)
        hash_password = post_password_hash.hexdigest()

        if delete_form.remove_file.data:

            #Post.objects(password=hash_password).update_one(unset__file=)
            pass

        else:
            post = Post.objects(password=hash_password)
            post.update(set__is_deleted=True)
            for item in post:
                delete_post_id = item.number
            redirect_path = '%s/thread/%s#%s' % (url, number, delete_post_id)

            return redirect(redirect_path)

    try:
        form = PostForm(name=session['name'])
        check_banned_list = Ban.objects(ip=int(client_ip))

        if check_banned_list:
            is_banned = True
            ban = check_banned_list
        else:
            is_banned = False
            ban = ''
        if is_banned:
            form = ''
            delete_form =''
        return render_template('thread.html',
                               ban=ban,
                               form=form,
                               all_boards = get_all_boards,
                               board=get_board,
                               delete_form=delete_form,
                               thread=get_thread,
                               posts=get_posts,
                               name=session['name'])
    except KeyError:
        check_banned_list = Ban.objects(ip=int(client_ip))

        if check_banned_list:
            is_banned = True
            ban = check_banned_list
        else:
            is_banned = False
            ban = ''

        if is_banned:
            form = ''
            delete_form = ''
        return render_template('thread.html',
                               ban=ban,
                               form=form,
                               all_boards = get_all_boards,
                               board=get_board,
                               delete_form=delete_form,
                               thread=get_thread,
                               posts=get_posts,
                               name='Anon')


@app.route('/<type>/image/<id>')
def images(id, type):
    image_from = None
    if type == 'op':
        image_from = Thread
    elif type == 'rp':
        image_from = Post
    get_image = image_from.objects(id=id).first()
    image = get_image.file.read()
    content_type = get_image.file.content_type

    return Response(image,
            content_type=content_type)
@app.route('/banner/image/')
def banners():
    get_banner = Banners.objects()
    get_random = random.choice(get_banner)
    id = get_random.id
    image = get_random.file.read()
    content_type = get_random.file.content_type
    return Response(image,
            content_type=content_type)

