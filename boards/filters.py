from core import app
from netaddr import IPAddress

@app.template_filter('date_normalize')
def date_normalize_filter(s):
    return s.strftime('%d %B %Y (%A) %H:%M:%S')

@app.template_filter('to_kb')
def to_kb(s):
    return s / 1024

@app.template_filter('is_long')
def is_long(s):
    if len(s) > 499:
        post = s[:499]
        return '%s... %s %s' % (post, "<div class='abbrev'>Post is long!",
                             "</div>")
    else:
        return s

@app.template_filter('ip_convert')
def ip_convert(s):
    int_ip = s
    address = IPAddress(int_ip)
    return address
