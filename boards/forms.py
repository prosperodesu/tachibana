from wtforms.fields import StringField, \
    TextAreaField,  \
    FileField, \
    BooleanField, \
    PasswordField
from wtforms.validators import Required, ValidationError
from flask_wtf import Form

class ThreadForm(Form):

    name = StringField('Name')
    subject = StringField('Subject')
    content = TextAreaField('Post', validators=[Required(message='Post is required!!')])

    file = FileField('Image', validators=[Required(message='Image is required!')])
    watermark = BooleanField(default=True)

class PostForm(Form):

    def validate_content(form, field):
        if not form.content.data:
            if not form.file.data:
                raise ValidationError('Post or Image is required!')

    name = StringField('Name')
    content = TextAreaField('Post', validators=[validate_content])

    file = FileField('Image')

    password = PasswordField('Password(for delete)')


class DeleteForm(Form):
    remove_file = BooleanField('Only File')
    password = PasswordField()