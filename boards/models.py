from mongoengine import *
from datetime import datetime

class Board(Document):
    name = StringField()
    url = StringField()
    pub_date = DateTimeField(default=datetime.now())
    threads = ListField(ReferenceField('Thread', dbref=True))

class Thread(Document):
    creator = StringField(default='Anon')
    number = SequenceField()
    subject = StringField()
    content = StringField()
    file = ImageField()
    img_width = IntField()
    img_height = IntField()
    thumb_width = IntField()
    thumb_height = IntField()
    img_size = IntField()
    reply = ListField(ReferenceField('Post'))
    pub_date = DateTimeField(default=datetime.now())
    bump_date = DateTimeField(default=datetime.now())
    reply_count = IntField(default=0)
    image_count = IntField(default=0)
    tripcode= StringField()
    ip = IntField()
    board = ReferenceField(Board, dbref = True)

    def __unicode__(self):
        return self.subject


class Post(Document):
    creator = StringField(default='Anon')
    number = SequenceField()
    content = StringField()
    file = ImageField()
    img_width = IntField()
    img_height = IntField()
    thumb_width = IntField()
    thumb_height = IntField()
    img_size = IntField()
    pub_date = DateTimeField(default=datetime.now())
    thread = ReferenceField(Thread, dbref = True)
    tripcode= StringField()
    password = StringField()
    is_deleted = BooleanField(default=False)

    ip = IntField()

    def __unicode__(self):
        return self.content

class Ban(Document):
    reason = StringField()
    start_date = DateTimeField(default=datetime.now())
    exp_date = DateTimeField()
    ip = IntField()

class Banners(Document):
    title = StringField()
    file = ImageField()
    pub_date = DateTimeField(default=datetime.now())


