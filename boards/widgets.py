from wtforms import TextAreaField

class CKTextAreaWidget(TextAreaField):
    def __call__(self, field, **kwargs):
        kwargs.setdefault('class_', 'cke')
        return super(CKTextAreaWidget, self).__call__(field, **kwargs)