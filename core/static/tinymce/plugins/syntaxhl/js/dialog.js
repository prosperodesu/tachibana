tinyMCEPopup.requireLangPack();

var SyntaxHLDialog = {
  wrapper: document.createElement('div'),
  init : function() {
  },

  insert : function() {
    var f = document.forms[0], textarea_output, options = '';

    //If no code just return.
    if(f.syntaxhl_code.value == '') {
      tinyMCEPopup.close();
      return false;
    }

    if(f.syntaxhl_nogutter.checked) {
      options += 'gutter: false; ';
    }
    if(f.syntaxhl_light.checked) {
      options += 'light: true; ';
    }
    if(f.syntaxhl_collapse.checked) {
      options += 'collapse: true; ';
    }
    if(f.syntaxhl_fontsize.value != '') {
      var fontsize=parseInt(f.syntaxhl_fontsize.value);
      options += 'fontsize: ' + fontsize + '; ';
    }

    if(f.syntaxhl_firstline.value != '') {
      var linenumber = parseInt(f.syntaxhl_firstline.value);
      options += 'first-line: ' + linenumber + '; ';
    }
    if(f.syntaxhl_highlight.value != '') {
      options += 'highlight: [' + f.syntaxhl_highlight.value + ']; ';
    }

    f.syntaxhl_code.value = f.syntaxhl_code.value.replace(/</g,'&lt;'); 
    f.syntaxhl_code.value = f.syntaxhl_code.value.replace(/>/g,'&gt;'); 
    textarea_output = '<pre><code '; 
    textarea_output += 'class="' + f.syntaxhl_language.value + '">'; 
    textarea_output += f.syntaxhl_code.value; 
    textarea_output += '</code></pre> '; /* note space at the end, had a bug it was inserting twice? */ 
    
    this.wrapper.innerHTML = textarea_output;
    hljs.highlightBlock(this.wrapper.firstChild.firstChild, '    ');
    tinyMCEPopup.editor.execCommand('mceInsertContent', false, this.wrapper.innerHTML); 
    tinyMCEPopup.close();
  }
};

tinyMCEPopup.onInit.add(SyntaxHLDialog.init, SyntaxHLDialog);
