__author__ = 'prospero'

from core import app
import logging
from logging import FileHandler
from settings import DEBUG, STATIC_DIR, mongodb_settings
from mongoengine import *
from boards.routes import site_boards
from pages.routes import site_index

if DEBUG == True:
    file_handler = FileHandler("debug.log","a")
    file_handler.setLevel(logging.WARNING)
    app.logger.addHandler(file_handler)
    from werkzeug import SharedDataMiddleware
    import os
    app.wsgi_app = SharedDataMiddleware(app.wsgi_app, {
      '/': STATIC_DIR
    })

connect(**mongodb_settings)

# Register Blueprints here
app.register_blueprint(site_index)
app.register_blueprint(site_boards)
#app.register_blueprint(site_page)