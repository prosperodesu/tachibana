from admin.contrib import admin
from core.contrib import app
import os
from boards.filters import date_normalize_filter, \
    to_kb, \
    is_long, \
    ip_convert


app.jinja_env.autoescape = False

# Run settings
autoreload = True
port = 8000
threaded=True

# App settings
app.secret_key = 'sa2342eda234214'
CSRF_ENABLED = True

#Debug settings
DEBUG = True

# Database Settings(MongoDB)
mongodb_settings = {
    'db':'myboard', # Database Name
    # 'username':None,
    # 'password':None,
    # 'host':None,
    # 'port':None
}


STATIC_DIR = os.path.join(os.path.dirname(__file__), 'static')
UPLOAD_FOLDER = os.path.join(os.path.dirname(__file__), 'upload')
IMAGES_FOLDER = os.path.join(os.path.dirname(__file__), 'images')
ALLOWED_EXTENSIONS = set(['jpg', 'gif', 'png', 'jpeg'])

app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
app.config['IMAGES_FOLDER'] = IMAGES_FOLDER

app.jinja_env.filters['data_normalize'] = date_normalize_filter
app.jinja_env.filters['to_kb'] = to_kb
app.jinja_env.filters['is_long'] = is_long
app.jinja_env.filters['ip_convert'] = ip_convert