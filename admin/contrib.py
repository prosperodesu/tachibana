from core import app
from flask_admin import Admin
from flask_admin.contrib.mongoengine import ModelView
from boards.models import Board, Thread, Post, Ban, Banners

admin = Admin(app)

admin.add_view(ModelView(Board, 'Boards'))
admin.add_view(ModelView(Thread, 'Threads'))
admin.add_view(ModelView(Post, 'Posts'))
admin.add_view(ModelView(Ban, 'Bans'))
admin.add_view(ModelView(Banners, 'Random Banners'))



